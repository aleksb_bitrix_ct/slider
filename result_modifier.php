<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

// Check paramameters
if(empty($arParams["SLIDER_TYPE"])) $arParams["SLIDER_TYPE"] = "horizontal";
if(empty($arParams["SPEED"])) $arParams["SPEED"] = 500;

// Width and height
$intWidthVal = intval($arParams['SL_WIDTH']);
$strWidthMeasure = str_replace($intWidthVal, '', $arParams['SL_WIDTH']);
if(empty($strWidthMeasure)) $arParams['SL_WIDTH'] .= 'px';

$intWidthVal = intval($arParams['SL_HEIGHT']);
$strWidthMeasure = str_replace($intWidthVal, '', $arParams['SL_HEIGHT']);
if(empty($strWidthMeasure)) $arParams['SL_HEIGHT'] .= 'px';