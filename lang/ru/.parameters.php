<?
$MESS ['SL_TYPE_VERTICAL'] = "Вертикальное";
$MESS ['SL_TYPE_HORIZONTAL'] = "Горизонтальное";

$MESS ['SLIDER_TYPE'] = "Тип пролистывания слайдера";
$MESS ['SL_WIDTH'] = "Ширина";
$MESS ['SL_HEIGHT'] = "Высота";
$MESS ['LOOP'] = "Зациклить";
$MESS ['SPEED'] = "Скорость";

$MESS ['USE_PAGINATION'] = "Использовать пагинацию";
$MESS ['PG_TYPE_BULLET'] = "bullets";
$MESS ['PG_TYPE_FRACTION'] = "fraction";
$MESS ['PG_TYPE_PROGRESSBAR'] = "progressbar";
$MESS ['PG_TYPE_CUSTOM'] = "custom";
$MESS ['PAGINATION_TYPE'] = "Тип пагинации";
$MESS ['PAGINATION_CLICKABLE'] = "Кликабельная пагинация";

$MESS ['USE_NAVIGATION'] = "Использовать навигацию";
$MESS ['USE_SCROLLBAR'] = "Использовать полосу прокрутки";