<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? if(!empty($arResult['ITEMS']) && !empty($arParams['IBLOCK_ID'])){ ?>

	<div class="swiper-container">
		
		<div class="swiper-wrapper">
			<? foreach($arResult['ITEMS'] as $arItem){ ?>

				<?
					$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
					$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
				?>

				<div class="swiper-slide" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
					<img src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>">
				</div>

			<? } ?>
		</div>
		
		<? if($arParams['USE_PAGINATION'] == "Y"){ ?>
			<div class="swiper-pagination"></div>
		<? } ?>

		<? if($arParams['USE_NAVIGATION'] == "Y"){ ?>
			<div class="swiper-button-prev"></div>
			<div class="swiper-button-next"></div>
		<? } ?>

		<? if($arParams['USE_SCROLLBAR'] == "Y"){ ?>
			<div class="swiper-scrollbar"></div>
		<? } ?>
		
	</div>

<? } ?>

<style>
    .swiper-container {
        width: <?=$arParams['SL_WIDTH']?>;
        height: <?=$arParams['SL_HEIGHT']?>;
    }
</style>

<script>
	var sl_direct = "<?=$arParams['SLIDER_TYPE']?>";
	var sl_loop = <?=($arParams['LOOP'] == "Y" ? 'true' : 'false');?>;
    var sl_speed = <?=intval($arParams['SPEED'])?>;

	var sl_pag = <?=($arParams['USE_PAGINATION'] == "Y" ? 'true' : 'false');?>;
	var sl_pag_type = "<?=$arParams['PAGINATION_TYPE']?>";
	var sl_pag_clickable = <?=($arParams['PAGINATION_CLICKABLE'] == "Y" ? 'true' : 'false');?>;

    var sl_nav = <?=($arParams['USE_NAVIGATION'] == "Y" ? 'true' : 'false');?>;
	var sl_scroll = <?=($arParams['USE_SCROLLBAR'] == "Y" ? 'true' : 'false');?>;
</script>