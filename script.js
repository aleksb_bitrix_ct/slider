$(function(){
	var params = {
		direction: sl_direct,
		loop: sl_loop,
		speed: sl_speed
	};
	if(sl_nav){
		params['navigation'] = {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
		};
	}
	if(sl_pag){
		params['pagination'] = {
            el: '.swiper-pagination',
            type: sl_pag_type,
            clickable: sl_pag_clickable,
		};
	}
	if(sl_scroll){
		params['scrollbar'] = {
            el: '.swiper-scrollbar',
		};
	}
	
	var mySwiper = new Swiper ('.swiper-container', params);
});