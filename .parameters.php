<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arSliderTypes = array(
    'horizontal' => GetMessage("SL_TYPE_HORIZONTAL"),
	'vertical' => GetMessage("SL_TYPE_VERTICAL")
);

$arTemplateParameters = array(
	"SLIDER_TYPE" => Array(
		"NAME" => GetMessage("SLIDER_TYPE"),
		"PARENT" => "VISUAL",
		"TYPE" => "LIST",
		"VALUES" => $arSliderTypes,
		"DEFAULT" => "horizontal",
	),
	"SL_WIDTH" => Array(
		"NAME" => GetMessage("SL_WIDTH"),
		"PARENT" => "VISUAL",
		"TYPE" => "STRING",
		"DEFAULT" => "600",
	),
	"SL_HEIGHT" => Array(
		"NAME" => GetMessage("SL_HEIGHT"),
		"PARENT" => "VISUAL",
		"TYPE" => "STRING",
		"DEFAULT" => "300",
	),
	"LOOP" => Array(
		"NAME" => GetMessage("LOOP"),
		"PARENT" => "VISUAL",
		"TYPE" => "CHECKBOX",
		"DEFAULT" => "N",
	),
    "SPEED" => Array(
        "NAME" => GetMessage("SPEED"),
        "PARENT" => "VISUAL",
        "TYPE" => "STRING",
        "DEFAULT" => "500",
    )
);

// Pagination
$arTemplateParameters["USE_PAGINATION"] = Array(
    "NAME" => GetMessage("USE_PAGINATION"),
    "PARENT" => "VISUAL",
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
    "REFRESH" => "Y"
);
if($arCurrentValues['USE_PAGINATION'] == 'Y'){
    $arPaginationTypes = array(
        'bullets' => GetMessage("PG_TYPE_BULLET"),
        'fraction' => GetMessage("PG_TYPE_FRACTION"),
        'progressbar' => GetMessage("PG_TYPE_PROGRESSBAR"),
        'custom' => GetMessage("PG_TYPE_CUSTOM")
    );
    $arTemplateParameters["PAGINATION_TYPE"] = Array(
        "NAME" => GetMessage("PAGINATION_TYPE"),
        "PARENT" => "VISUAL",
        "TYPE" => "LIST",
        "VALUES" => $arPaginationTypes,
        "DEFAULT" => "bullets",
        "REFRESH" => "Y"
    );
    if($arCurrentValues['PAGINATION_TYPE'] == 'bullets'){
        $arTemplateParameters["PAGINATION_CLICKABLE"] = Array(
            "NAME" => GetMessage("PAGINATION_CLICKABLE"),
            "PARENT" => "VISUAL",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N"
        );
    }
}

// Navigation
$arTemplateParameters["USE_NAVIGATION"] = Array(
    "NAME" => GetMessage("USE_NAVIGATION"),
    "PARENT" => "VISUAL",
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);

// Scrollbar
$arTemplateParameters["USE_SCROLLBAR"] = Array(
    "NAME" => GetMessage("USE_SCROLLBAR"),
    "PARENT" => "VISUAL",
    "TYPE" => "CHECKBOX",
    "DEFAULT" => "N",
);